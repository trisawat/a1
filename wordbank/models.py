from django.db import models

# Create your models here.
class word(models.Model):
    word_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.word_text


class details(models.Model):
    word = models.ForeignKey(word, on_delete=models.CASCADE)
    type_text = models.CharField(max_length=50)
    meaning_text = models.CharField(max_length=200)
    sentence_text = models.CharField(max_length=500)