from django.conf.urls import url

from . import views

app_name = 'wordbank'
urlpatterns = [
    url(r'^$', views.Home_page.as_view(), name='home_page'),
    ####################### latest 5 word######################
    url(r'^latest/$', views.latest_word.as_view(), name='latest_word'),
    ######################## all word #######################
    url(r'^all/$', views.AllView.as_view(), name='all_word'),
    url(r'^into_add_page/$', views.into_add_page, name='add_new_word'),
    url(r'^saveword/$', views.SaveWord, name='saveword'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='details'),
]
