import csv
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone



from .models import word, details



class Home_page(generic.ListView):
    template_name = 'wordbank/home_page.html'
    context_object_name = 'latest_word_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return word.objects.order_by('-pub_date')[:5]
   
class latest_word(generic.ListView):
    template_name = 'wordbank/latest_word.html'
    context_object_name = 'latest_word_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return word.objects.order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model = word
    template_name = 'wordbank/details.html'
    def get_queryset(self):
       return word.objects.filter(pub_date__lte=timezone.now())

class AllView(generic.ListView):
    template_name = 'wordbank/all_word.html'
    context_object_name = 'all_word_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return word.objects.order_by('word_text').all

def into_add_page(request):
    return render(request,'wordbank/add_new_word.html')

def SaveWord(request):

    if request.method == 'POST':
        add_word = request.POST['add_word']
        add_type = request.POST['add_type']
        add_mean = request.POST['add_mean']
        add_sentence = request.POST['add_sentence']
        add_datetime = timezone.now()

        new_word = word(
                word_text = add_word,
                pub_date  = add_datetime
                )
        new_word.save()

        new_details = details(
                word = new_word,
                type_text = add_type,
                meaning_text = add_mean,
                sentence_text = add_sentence
                )
        new_details.save()
    return HttpResponseRedirect(reverse('wordbank:home_page'))



       
       
       
        
            
          
 


    
